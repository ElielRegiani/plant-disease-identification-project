from django.core.exceptions import NON_FIELD_ERRORS
from keras.layers.serialization import serialize
from rest_framework.views import APIView
from .serializers import UserSerializer, PositioningInfoSerializer, ReplySerializer, RequestSerializer
from .models import User, Request, PositioningInfo, Reply
from rest_framework.decorators import action, api_view, parser_classes
from rest_framework import viewsets
from django.shortcuts import get_object_or_404
from django.core.exceptions import NON_FIELD_ERRORS
from rest_framework import serializers
from .serializers import UserSerializer, PositioningInfoSerializer, ReplySerializer, RequestSerializer
from rest_framework.parsers import FormParser, MultiPartParser
from .models import User
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import APIView
from rest_framework import generics
from rest_framework import mixins
from rest_framework import viewsets
from django.shortcuts import get_object_or_404
# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class RequestViewSet(viewsets.ModelViewSet):
    queryset = Request.objects.all()
    serializer_class = RequestSerializer
    parser_classes = [MultiPartParser, FormParser]

    @action(detail=True, methods=['post'])
    def post(self, request, format=None):
        print(request.data)
        serializer = RequestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PositioningInfoViewSet(viewsets.ModelViewSet):
    queryset = PositioningInfo.objects.all()
    serializer_class = PositioningInfoSerializer

class ReplyViewSet(viewsets.ModelViewSet):
    queryset = Reply.objects.all()
    serializer_class = ReplySerializer     
    