from typing import Match
from django.db import models
from django.utils.translation import gettext_lazy as _

def upload_to(instance, filename):
    return 'posts/{filename}'.format(filename=filename)

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=100)

    # Methods

    def __str__(self):
        return self.name

class Request(models.Model):
    
    image = models.ImageField(_("image"), upload_to=upload_to, default = 'post')

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    # Methods

class PositioningInfo(models.Model):
    latitude = models.FloatField(null= True)
    longitude = models.FloatField(null= True)
    temperature = models.FloatField(null= True)
    temperature_max = models.FloatField(null= True)
    temperature_min = models.FloatField(null= True)
    humidity = models.FloatField(null= True)
    weather = models.CharField(max_length=50, null= True)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)

    user = models.OneToOneRel(User, field_name="", to='')

    # Methods

    def getCity(self):
        return self.city
    
    def getState(self):
        return self.state

class Reply(models.Model):
    message = models.TextField(max_length=2000)
    calculatedValues = models.FloatField()
    climateInfo = models.FloatField()
    cultureInfo = models.FloatField()

    request = models.OneToOneRel(Request, field_name="", to='')

    # Methods